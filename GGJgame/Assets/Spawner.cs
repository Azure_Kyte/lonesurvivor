﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public List<GameObject> items;

    public bool Generate()
    {
        Ray ray = new Ray(transform.position + new Vector3(Random.Range(15f, -15f), 0, Random.Range(15f, -15f)), Vector3.down);
        RaycastHit rch;
        Physics.Raycast(ray, out rch, 30f);
        if (rch.collider != null)
        {
            if (rch.point.y < 0)
            {
                GameObject g = Instantiate(items[Random.Range(0, items.Count)], rch.point + new Vector3(0, 0.2f, 0), Quaternion.Euler(0, Random.Range(0f, 360f), 0));
                g.name = "item_" + name + "_" + Time.time.ToString();
                return true;
            }
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
