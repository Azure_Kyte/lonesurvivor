﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureFadeScript : MonoBehaviour
{

    Material m;
    float timer;
    MeshRenderer r;
    Color c;
    bool direction;
    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<MeshRenderer>();
        m = new Material(r.material);
        c = m.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (!direction)
        {
            timer += Time.deltaTime * 0.5f;
            if (timer > 1)
            {
                timer = 1;
                direction = !direction;
            }
        } else
        {
            timer -= Time.deltaTime * 0.5f;
            if (timer < 0)
            {
                timer = 0;
                direction = !direction;
            }
        }

        c.a = Mathf.SmoothStep(0f, 1f, timer);
        m.color = c;
        r.material = m;
    }
}
