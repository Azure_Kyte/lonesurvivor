﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conditions : MonoBehaviour
{
    [System.Serializable]
    public class Condition
    {
        public string name;
        public int currentValue;
        public int maxValue;
        public int originalValue;
        public Sprite icon;

        public enum ConditionType {Oxygen, Health, Integrity, Battery};
        public ConditionType conditionType;

        public Color ConditionColor;
       
    };

    public bool canMove;

    public List<Condition> ConditionList = new List<Condition>();

    // Start is called before the first frame update
    void Start()
    {
        foreach(Condition c  in ConditionList)
        {
            c.originalValue = c.maxValue;
        }
    }

    public void ProcessConditions()
    {
        foreach (Condition c in ConditionList)
        {
            if (c.conditionType == Condition.ConditionType.Oxygen)
            {

                c.maxValue = c.originalValue + (30 * InventorySystem.Inventory._activeInventory.ExistsInInventory(8, true));

                canMove = true;

                if (c.currentValue > 0)
                    c.currentValue--;
                else
                {
                    foreach(Condition cc in ConditionList)
                    {
                        if (cc.conditionType == Condition.ConditionType.Health)
                        {
                            if (cc.currentValue > 0)
                                cc.currentValue--;
                            else
                            {
                                //GAME OVER

                            }
                        }
                    }
                }
            }

            if (c.conditionType == Condition.ConditionType.Battery)
            {
                c.maxValue = c.originalValue + (30 * InventorySystem.Inventory._activeInventory.ExistsInInventory(0, true));

                canMove = true;
                if (c.currentValue >= 0)
                    c.currentValue--;
                else
                    canMove = false;
            }
        }
    }

    public Condition FetchCondition(Condition.ConditionType type)
    {
        foreach(Condition c in ConditionList)
        {
            if (type == c.conditionType)
                return c;
        }
        return null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
