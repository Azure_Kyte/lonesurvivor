﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FoundryClick : MonoBehaviour, IPointerClickHandler
{

    public int mode;
    public void OnPointerClick(PointerEventData eventData)
    {
        BaseOptions.mode = mode;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
