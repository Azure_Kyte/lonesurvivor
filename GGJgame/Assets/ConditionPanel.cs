﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConditionPanel : MonoBehaviour
{

    public int conditionID;
    public Image panel;
    public Image icon;
    public Image barOutline;
    public Image bar;
    public TMPro.TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (conditionID < ControllableObject._activeObject.conditionsScript.ConditionList.Count)
        {
            panel.enabled = true;
            icon.enabled = true;
            barOutline.enabled = true;
            bar.enabled = true;
            text.enabled = true;

            Conditions.Condition c = ControllableObject._activeObject.conditionsScript.ConditionList[conditionID];

            icon.sprite = c.icon;
            bar.fillAmount = Mathf.InverseLerp(0f, c.maxValue, c.currentValue);
            text.text = c.name;
            bar.color = c.ConditionColor;
           
        } else
        {
            panel.enabled = false;
            icon.enabled = false;
            barOutline.enabled = false;
            bar.enabled = false;
            text.enabled = false;
        }
    }
}
