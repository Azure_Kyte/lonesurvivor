﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

    public static Transform _target;

    public int baseID;

    float timer;

    public Vector3 targetPosition;
    public Vector3 targetRotation;
    // Start is called before the first frame update
    void Start()
    {
        _target = ControllableObject._activeObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (_target != null)
        {
            ControllableObject c = _target.GetComponent<ControllableObject>();
            if (!c.inBase) {

                timer -= Time.deltaTime * 4f;
                if (timer < 0)
                    timer = 0;
            }
            else
            {
                timer += Time.deltaTime * 3f;
                if (timer > 1)
                    timer = 1;
                if (c.BaseID == 0) {
                    targetPosition = new Vector3(0, 16, 25);
                    targetRotation = new Vector3(47, -140, 0);
                } else if (c.BaseID == 1)
                {
                    targetPosition = new Vector3(78.57f, 9.57f, 33f);
                    targetRotation = new Vector3(30, -48, 0);
                }
            }

            transform.position = Vector3.Lerp(_target.transform.position + new Vector3(0, 18, -7), targetPosition, Mathf.SmoothStep(0f, 1f, timer));
            transform.rotation = Quaternion.Euler(Vector3.Lerp(new Vector3(66, 0, 0), targetRotation, Mathf.SmoothStep(0f, 1f, timer)));

        }
    }
}
