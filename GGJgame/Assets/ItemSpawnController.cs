﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawnController : MonoBehaviour
{
    public int targetItems;
    public static int items = 0;

    public List<GameObject> spawners;

    public static ItemSpawnController _controller;

    int currentNum;


    // Start is called before the first frame update
    void Start()
    {
        _controller = this;
        while (currentNum < targetItems)
        {
            int spawner = Random.Range(0, spawners.Count);

            bool success = spawners[spawner].GetComponent<Spawner>().Generate();

            if (success) {
                items++;
                currentNum++;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void GenerateToTarget()
    {
        currentNum = items;
        while (currentNum < targetItems)
        {
            int spawner = Random.Range(0, spawners.Count);

            bool success = spawners[spawner].GetComponent<Spawner>().Generate();

            if (success)
            {
                items++;
                currentNum++;
            }
        }
    }
}
