﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using InventorySystem;

public class ShipInteraction : MonoBehaviour
{

    public ShipQualityScript sqs;
    public TextMeshPro text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void RepairShip(Inventory.InventoryItem i)
    {
        sqs.currentQuality += i.item.value * i.quantity;

        text.text = (sqs.currentQuality * 100f).ToString("0.00") + "%";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
