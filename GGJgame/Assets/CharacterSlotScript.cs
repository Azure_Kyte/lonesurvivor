﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class CharacterSlotScript : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (unlocked)
        {
            foreach(TextMeshProUGUI t in ControllableObjectsUI._UITexts)
            {
                if (t.GetHashCode() == text.GetHashCode())
                {
                    if (!t.text.Contains("(*)"))
                    {
                        t.text += " (*)";
                    }
                }
                else
                {
                    t.text = t.text.Replace(" (*)", "");
                }
            }

          
            ControllableObject._activeObject = ControllableObjectsUI.controllables[id];

            InventorySystem.Inventory._activeInventory = ControllableObject._activeObject.GetComponent<InventorySystem.Inventory>();
            InventorySystem.Inventory._activeInventory.UpdateInventoryView();
        }
    }

    bool unlocked;
    public int id;

    public Image mainIcon;
    public Image subIcon;


    public TextMeshProUGUI text; 

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UnlockCheck", 0f, 1.0f);

        ControllableObjectsUI._UITexts.Add(text);
    }

    void UnlockCheck()
    {
        unlocked = (id < ControllableObjectsUI.controllables.Count && id >= 0);

    }

    // Update is called once per frame
    void Update()
    {
        if (unlocked)
        {
            mainIcon.enabled = true;
            subIcon.enabled = true;
            text.enabled = true;
        }
    }
}
