﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace InventorySystem
{
    public class Inventory : MonoBehaviour
    {

        [System.Serializable]
        public class InventoryItem {

            public Item item;
            public int quantity;
        };

        public int maxInventorySize;
        public bool baseInventory;
        public bool foundryItem;

        public bool targetsBases;
        public static Inventory _targetInventory;

        public static Inventory _activeInventory;
        public static int context; // 0 = No context/Normal, 1 = Interacting with Foundry, 2 = Interacting with Base Inventory

        public List<InventoryItem> _inventory = new List<InventoryItem>();

        // Start is called before the first frame update
        void Start()
        {
            if (name == "Human")
            {
                _activeInventory = this;
                UpdateInventoryView();
            }
            if (name == "InventoryPanel")
                ItemHoverScript._baseInventory = GetComponent<Inventory>();
        }

        public bool ExistsInInventory(int id)
        {
            foreach(InventoryItem i in _inventory)
            {
                if (id == i.item.id)
                    return true;
            }
            return false;
        }

        public int ExistsInInventory(int id, bool returnQuantity)
        {
            foreach (InventoryItem i in _inventory)
            {
                if (id == i.item.id)
                {
                    return i.quantity;
                }
                   
            }
            return 0;
        }

        // Update is called once per frame
        void Update()
        {
            context = BaseOptions.mode;
            
        }

        public void UpdateInventoryView()
        {
            for (int i = 0; i < maxInventorySize; i++)
            {
                if (!baseInventory)
                {
                    if (!foundryItem) { 
                        Image g = GameObject.Find("Item_" + (i + 1).ToString()).GetComponent<Image>();
                        if (i < _inventory.Count)
                        {
                            if (_inventory[i].item.icon != null)
                                g.sprite = _inventory[i].item.icon;
                            else
                                g.sprite = ItemList._noSprite;
                        }
                        else
                        {
                            g.sprite = ItemList._noItem;

                        }
                    }
                    else
                    {
                        Image g = GetComponent<Image>();
                        if (i < _inventory.Count)
                        {
                            if (_inventory[i].item.icon != null)
                                g.sprite = _inventory[i].item.icon;
                            else
                                g.sprite = ItemList._noSprite;
                        }
                        else
                        {
                            g.sprite = ItemList._noItem;

                        }
                    }
                } else
                {

                    
                    Image g = GameObject.Find("InventorySlot" + (i + 1).ToString()).GetComponent<Image>();
                    if (i < _inventory.Count)
                    {
                        if (_inventory[i].item.icon != null)
                            g.sprite = _inventory[i].item.icon;
                        else
                            g.sprite = ItemList._noSprite;
                    }
                    else
                    {
                        g.sprite = ItemList._noItem;

                    }
                }

            }
        }

        public static void TransferItemToInventory(int id, int quantity, Inventory fromInv, Inventory toInv)
        {
            if (ItemList._items.Count > id && id >= 0)
            {
                if (quantity > 0)
                {

                    int existsInfromInventory = -1;
                    foreach (InventoryItem i in fromInv._inventory)
                    {
                        if (i.item.id == id)
                        {
                            existsInfromInventory = fromInv._inventory.IndexOf(i);
                        }
                    }

                    int existsIntoInventory = -1;
                    foreach (InventoryItem i in toInv._inventory)
                    {
                        if (i.item.id == id)
                        {
                            existsIntoInventory = toInv._inventory.IndexOf(i);
                        }
                    }

                    // Item exists in From Inventory List, transfer to target Inventory
                    if (existsInfromInventory > -1)
                    {

                        int q = 0;
                        if (fromInv._inventory[existsInfromInventory].quantity < quantity)
                            q = fromInv._inventory[existsInfromInventory].quantity;
                        else
                            q = quantity;

                        //Item exists in target Inventory
                        if (existsIntoInventory > -1)
                        {

                            //Add the quantity to the target inventory, remove item from origin inventory if quantity is zero (or less, which shouldn't happen) 
                            toInv._inventory[existsIntoInventory].quantity += q;
                            fromInv._inventory[existsInfromInventory].quantity -= q;
                            if (fromInv._inventory[existsInfromInventory].quantity <= 0)
                                fromInv._inventory.RemoveAt(existsInfromInventory);
                            return;
                        }
                        //Item does not exist in target Inventory
                        else
                        {
                            if (toInv._inventory.Count < toInv.maxInventorySize)
                            {
                                toInv.AddItemToInventory(id, q);
                                fromInv._inventory[existsInfromInventory].quantity -= q;
                                if (fromInv._inventory[existsInfromInventory].quantity <= 0)
                                    fromInv._inventory.RemoveAt(existsInfromInventory);
                                return;
                            }
                        }
                    }

                    //Item doesn't even exist in the original inventory. What the heck. Do nothing.
                    
                }
            }
            return;
        }

        public bool AddItemToInventory(int id, int quantity)
        {
            if (ItemList._items.Count > id && id >= 0)
            {
                if (quantity > 0)
                {
                    int existsInInventory = -1;
                    foreach(InventoryItem i in _inventory)
                    {
                        if (i.item.id == id)
                        {
                            existsInInventory = _inventory.IndexOf(i);
                            i.quantity += quantity;
                            UpdateInventoryView();
                            return true;
                        }
                    }

                    if (existsInInventory == -1)
                    {
                        if (_inventory.Count < maxInventorySize)
                        {
                            InventoryItem i = new InventoryItem();
                            i.item = ItemList._items[id];
                            i.quantity = quantity;
                            _inventory.Add(i);
                            UpdateInventoryView();
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}