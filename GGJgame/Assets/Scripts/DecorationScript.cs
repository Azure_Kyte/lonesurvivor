﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorationScript : MonoBehaviour
{

    public Transform origin;
    public List<GameObject> objs;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 100; i++)
        {
            Ray r = new Ray(origin.position + new Vector3(Random.Range(100f, -100f), 10f, Random.Range(100f,-100f)), Vector3.down);
            RaycastHit rch;
            Physics.Raycast(r, out rch,35f);
            if (rch.collider != null)
            {

                if (rch.collider.tag == "Terrain")
                {
                    GameObject g = Instantiate(objs[Random.Range(0, objs.Count)], rch.point, Quaternion.Euler(0, Random.Range(0f, 360f), 0), origin);
                    int ii = Random.Range(3, 6);
                    g.transform.localScale = new Vector3(ii, ii, ii);
                }
            }
           
        }
    }

}
