﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystem
{
   

    
    [System.Serializable]
    public class ItemList : MonoBehaviour
    {

        public Sprite noSprite;
        public static Sprite _noSprite;

        public Sprite noItem;
        public static Sprite _noItem;


        public List<Item> itemList;
        public static List<Item> _items;
        // Start is called before the first frame update
        void Awake()
        {
            for (int i = 0; i < itemList.Count; i++)
            {
                itemList[i].id = i;
            }
            _noSprite = noSprite;
            _noItem = noItem;
            _items = itemList;
        }
    }
}