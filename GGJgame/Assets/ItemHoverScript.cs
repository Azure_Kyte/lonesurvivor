﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using InventorySystem;

public class ItemHoverScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    public int itemId;

    public Inventory specificInventory;
    public static Inventory _baseInventory;

    public void OnPointerClick(PointerEventData eventData)
    {


        if (BaseOptions.mode == 0)
        {
            Ray ray = new Ray(ControllableObject._activeObject.transform.position, ControllableObject._activeObject.transform.forward);
            RaycastHit rch;
            Physics.Raycast(ray, out rch, 1f);
            
            if (rch.collider != null)
            {
                if (rch.collider.GetComponent<ControllableObject>())
                {
                    if (eventData.button == PointerEventData.InputButton.Left)
                    {
                        Inventory targetInventory = rch.collider.GetComponent<Inventory>();
                        if (targetInventory._inventory.Count < targetInventory.maxInventorySize || targetInventory.ExistsInInventory(Inventory._activeInventory._inventory[itemId].item.id))
                        {
                            int wasBattery = 0;
                            if (Inventory._activeInventory._inventory[itemId].item.id == 0) wasBattery = Inventory._activeInventory._inventory[itemId].quantity;

                            int wasOxygen = 0;
                            if (Inventory._activeInventory._inventory[itemId].item.id == 8) wasOxygen = Inventory._activeInventory._inventory[itemId].quantity;

                            Inventory.TransferItemToInventory(Inventory._activeInventory._inventory[itemId].item.id, Inventory._activeInventory._inventory[itemId].quantity, Inventory._activeInventory, targetInventory);
                            
                            if (wasBattery > 0)
                            {
                                Conditions c = targetInventory.GetComponent<ControllableObject>().conditionsScript;

                                foreach(Conditions.Condition cc in c.ConditionList)
                                {
                                    if (cc.conditionType == Conditions.Condition.ConditionType.Battery)
                                    {
                                        cc.maxValue = cc.originalValue + targetInventory.ExistsInInventory(0, true) * 30;
                                        cc.currentValue += wasBattery * 15;
                                        wasBattery = 0;
                                    }

                                    if (cc.conditionType == Conditions.Condition.ConditionType.Oxygen)
                                    {
                                        cc.maxValue = cc.originalValue + (targetInventory.ExistsInInventory(0, true) * 30);
                                        cc.currentValue += wasOxygen * 25;
                                        wasOxygen = 0;
                                    }
                                }
                            }
                        }

                        
                    }
                    else if (eventData.button == PointerEventData.InputButton.Right)
                    {
                        int q = Inventory._activeInventory._inventory[itemId].quantity;
                        if (q > 1)
                            q = q / 2;


                        Inventory targetInventory = rch.collider.GetComponent<Inventory>();
                        if (targetInventory._inventory.Count < targetInventory.maxInventorySize || targetInventory.ExistsInInventory(Inventory._activeInventory._inventory[itemId].item.id))
                        {
                            Inventory.TransferItemToInventory(Inventory._activeInventory._inventory[itemId].item.id, Inventory._activeInventory._inventory[itemId].quantity, Inventory._activeInventory, targetInventory);
                        }
                    }
                    Inventory._activeInventory.UpdateInventoryView();
                } else if (rch.collider.GetComponent<ShipInteraction>())
                {
                    if (Inventory._activeInventory._inventory.Count > itemId)
                    {
                        if (Inventory._activeInventory._inventory[itemId].item.value > 0)
                        {
                            ShipInteraction s = rch.collider.GetComponent<ShipInteraction>();
                            s.RepairShip(Inventory._activeInventory._inventory[itemId]);

                            Inventory._activeInventory._inventory.RemoveAt(itemId);
                            Inventory._activeInventory.UpdateInventoryView();
                        }
                    }
                }
            }

        } else if (BaseOptions.mode == 1) // Foundry Screen
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                //Clicking current Inventory
                if (specificInventory == null ) {

                    if (Inventory._activeInventory._inventory.Count > itemId)
                    {
                        if (ProcessButton.foundry.slot1._inventory.Count < 1 || ProcessButton.foundry.slot1._inventory[0].item.id == Inventory._activeInventory._inventory[itemId].item.id)
                        {
                            Inventory.TransferItemToInventory(Inventory._activeInventory._inventory[itemId].item.id,
                                Inventory._activeInventory._inventory[itemId].quantity, Inventory._activeInventory, ProcessButton.foundry.slot1);
                        }
                        else if (ProcessButton.foundry.slot2._inventory.Count < 1 || ProcessButton.foundry.slot2._inventory[0].item.id == Inventory._activeInventory._inventory[itemId].item.id)
                        {
                            Inventory.TransferItemToInventory(Inventory._activeInventory._inventory[itemId].item.id,
                                Inventory._activeInventory._inventory[itemId].quantity, Inventory._activeInventory, ProcessButton.foundry.slot2);
                        }
                        ProcessButton.foundry.ProcessAllUI();
                        Inventory._activeInventory.UpdateInventoryView();
                    }
                }

                else
                {
                    if (specificInventory._inventory.Count > itemId)
                    {
                        if (Inventory._activeInventory._inventory.Count < Inventory._activeInventory.maxInventorySize || Inventory._activeInventory.ExistsInInventory(specificInventory._inventory[itemId].item.id))
                        {
                            Inventory.TransferItemToInventory(specificInventory._inventory[itemId].item.id, specificInventory._inventory[itemId].quantity,
                                specificInventory, Inventory._activeInventory);
                        }
                        Inventory._activeInventory.UpdateInventoryView();
                        specificInventory.UpdateInventoryView();
                    }
                }
                DescriptionWindowScript._target = -1;
                DescriptionWindowScript._target = itemId;
            }

            else if (eventData.button == PointerEventData.InputButton.Right)
            {
                //Clicking current Inventory
                if (specificInventory == null)
                {
                    if (Inventory._activeInventory._inventory.Count > itemId)
                    {
                        int q = Inventory._activeInventory._inventory[itemId].quantity;
                        if (q > 1)
                            q = q / 2;

                        if (ProcessButton.foundry.slot1._inventory.Count < 1 || ProcessButton.foundry.slot1._inventory[0].item.id == Inventory._activeInventory._inventory[itemId].item.id)
                        {
                            Inventory.TransferItemToInventory(Inventory._activeInventory._inventory[itemId].item.id,
                                q, Inventory._activeInventory, ProcessButton.foundry.slot1);
                        }
                        else if (ProcessButton.foundry.slot2._inventory.Count < 1 || ProcessButton.foundry.slot2._inventory[0].item.id == Inventory._activeInventory._inventory[itemId].item.id)
                        {
                            Inventory.TransferItemToInventory(Inventory._activeInventory._inventory[itemId].item.id,
                                q, Inventory._activeInventory, ProcessButton.foundry.slot2);
                        }
                        Inventory._activeInventory.UpdateInventoryView();
                        ProcessButton.foundry.ProcessAllUI();
                    }
                }
                else
                {
                    if (specificInventory._inventory.Count > itemId)
                    {

                        int q = specificInventory._inventory[itemId].quantity;
                        if (q > 1)
                            q = q / 2;

                        if (Inventory._activeInventory._inventory.Count < Inventory._activeInventory.maxInventorySize || Inventory._activeInventory.ExistsInInventory(specificInventory._inventory[itemId].item.id))
                        {
                            Inventory.TransferItemToInventory(specificInventory._inventory[itemId].item.id, q,
                                specificInventory, Inventory._activeInventory);
                        }
                        specificInventory.UpdateInventoryView();
                        ProcessButton.foundry.ProcessAllUI();
                    }
                }
                DescriptionWindowScript._target = -1;
                DescriptionWindowScript._target = itemId;
            }
        } else if (BaseOptions.mode == 2) // Base Inventory
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                //Clicking current Inventory
                if (specificInventory == null)
                {

                    if (Inventory._activeInventory._inventory.Count > itemId)
                    {
                        if (_baseInventory._inventory.Count < _baseInventory.maxInventorySize || specificInventory._inventory[0].item.id == Inventory._activeInventory._inventory[itemId].item.id)
                        {
                            Inventory.TransferItemToInventory(Inventory._activeInventory._inventory[itemId].item.id,
                                Inventory._activeInventory._inventory[itemId].quantity, Inventory._activeInventory, _baseInventory);
                        }

                        _baseInventory.UpdateInventoryView();
                        Inventory._activeInventory.UpdateInventoryView();
                    }
                }

                else
                {
                    if (specificInventory._inventory.Count > itemId)
                    {
                        if (Inventory._activeInventory._inventory.Count < Inventory._activeInventory.maxInventorySize || Inventory._activeInventory.ExistsInInventory(specificInventory._inventory[itemId].item.id))
                        {
                            Inventory.TransferItemToInventory(specificInventory._inventory[itemId].item.id, specificInventory._inventory[itemId].quantity,
                                specificInventory, Inventory._activeInventory);
                        }

                        specificInventory.UpdateInventoryView();
                        Inventory._activeInventory.UpdateInventoryView();
                    }
                }
                DescriptionWindowScript._target = -1;
                DescriptionWindowScript._target = itemId;
            }

            else if (eventData.button == PointerEventData.InputButton.Right)
            {
                //Clicking current Inventory
                if (specificInventory == null)
                {
                    if (Inventory._activeInventory._inventory.Count > itemId)
                    {
                        int q = Inventory._activeInventory._inventory[itemId].quantity;
                        if (q > 1)
                            q = q / 2;

                        if (_baseInventory._inventory.Count < _baseInventory.maxInventorySize || _baseInventory._inventory[0].item.id == Inventory._activeInventory._inventory[itemId].item.id)
                        {
                            Inventory.TransferItemToInventory(Inventory._activeInventory._inventory[itemId].item.id,
                                q, Inventory._activeInventory, _baseInventory);
                        }
                        _baseInventory.UpdateInventoryView();
                        Inventory._activeInventory.UpdateInventoryView();
                    }
                }
                else
                {
                    if (specificInventory._inventory.Count > itemId)
                    {

                        int q = specificInventory._inventory[itemId].quantity;
                        if (q > 1)
                            q = q / 2;

                        if (Inventory._activeInventory._inventory.Count < Inventory._activeInventory.maxInventorySize || Inventory._activeInventory.ExistsInInventory(specificInventory._inventory[itemId].item.id))
                        {
                            Inventory.TransferItemToInventory(specificInventory._inventory[itemId].item.id, q,
                                specificInventory, Inventory._activeInventory);
                        }
                        specificInventory.UpdateInventoryView();
                    }
                }
                DescriptionWindowScript._target = -1;
                DescriptionWindowScript._target = itemId;
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (specificInventory == null)
        {
            DescriptionWindowScript.targetInventory = InventorySystem.Inventory._activeInventory;
            DescriptionWindowScript._target = itemId;
        }
        else
        {
            DescriptionWindowScript.targetInventory = specificInventory;
            DescriptionWindowScript._target = itemId;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
         DescriptionWindowScript._target = -1;
    }

    // Start is called before the first frame update
    void Start()
    {



        if (specificInventory != null)
        {
            if (name.Contains("InventorySlot"))
                itemId = int.Parse(name.Replace("InventorySlot", "")) - 1;

        } 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
