﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystem
{
    [System.Serializable]
    public class Item
    {
        public int id;
        public string name;
        public string description;
        public Sprite icon;
        public float value;
    }
}