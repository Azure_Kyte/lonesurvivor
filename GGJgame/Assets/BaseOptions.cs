﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseOptions : MonoBehaviour
{

    public static int mode = 0; // 0 = none, 1 = Foundry, 2 = Inventory
    public static int currentBase;

    public GameObject foundry;
    public GameObject inventory;
    public GameObject Button1;
    public GameObject Button2;

    // Update is called once per frame
    void Update()
    {
        if (ControllableObject._activeObject.inBase)
        {
            Button1.SetActive(true);
            Button2.SetActive(true);
             if (mode == 1)
            {
                foundry.SetActive(true);
                inventory.SetActive(false);
            }
            else if (mode == 2)
            {
                foundry.SetActive(false);
                inventory.SetActive(true);
            }

        } else
        {
            Button1.SetActive(false);
            Button2.SetActive(false);
        }


        if (mode == 0)
        {
            foundry.SetActive(false);
            inventory.SetActive(false);
        }


    }
}
