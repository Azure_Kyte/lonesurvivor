﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipScript : MonoBehaviour
{
    float timer = 0;

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0, 0, 5) * Time.deltaTime;
        transform.rotation *= Quaternion.Euler(0, 0, 5 * Time.deltaTime);

        timer += Time.deltaTime;

        if (timer > 20f)
            Application.Quit(0);
    }
}
