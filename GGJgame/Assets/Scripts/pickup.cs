﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickup : MonoBehaviour
{

    public int itemID;
    public int quantMax;
    public int quantMin;

    public bool startedHere;

    private void Start()
    {
        if (startedHere)
        {
            ItemSpawnController.items++;
        }
    }

    private void OnDestroy()
    {
        ItemSpawnController.items--;
    }
}
