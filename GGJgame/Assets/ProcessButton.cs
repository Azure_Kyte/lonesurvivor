﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using InventorySystem;

public class ProcessButton : MonoBehaviour, IPointerClickHandler
{

    public static ProcessButton foundry;

    public Inventory slot1;
    public Inventory slot2;
    public Inventory output;


    public void ProcessAllUI()
    {
        slot1.UpdateInventoryView();
        slot2.UpdateInventoryView();
        output.UpdateInventoryView();
    }

    public void Awake()
    {
        foundry = this;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        Inventory.InventoryItem slot1Item = null;
        Inventory.InventoryItem slot2Item = null;
        Inventory.InventoryItem outputItem = null;

        if (slot1._inventory.Count  == 1)
        {
            slot1Item = slot1._inventory[0];
        }
        if (slot2._inventory.Count == 1)
        {
            slot2Item = slot2._inventory[0];
        }

        if (output._inventory.Count == 1)
        {
            outputItem = output._inventory[0];
        }

        //No Output currently in slot.
        if (outputItem == null)
        {
           
            //Move slot 2 to slot 1 if 1 is empty.
           if (slot1Item == null && slot2Item != null)
            {
                Inventory.TransferItemToInventory(slot2._inventory[0].item.id, slot2Item.quantity, slot2, slot1);
                slot1Item = slot1._inventory[0];
                slot2Item = null;
            }
            
            if (slot1Item != null && slot2Item == null)
            {
                //Metal Plate
                if (slot1Item.item.id == 1 && slot2Item == null)
                {
                    output.AddItemToInventory(3, slot1Item.quantity);
                    slot1._inventory.Clear();
                }
                //Metal Plate
                if (slot1Item.item.id == 7 && slot2Item == null)
                {
                    output.AddItemToInventory(3, slot1Item.quantity);
                    slot1._inventory.Clear();
                }
                //Glass
                if (slot1Item.item.id == 4 && slot2Item == null)
                {
                    output.AddItemToInventory(6, slot1Item.quantity);
                    slot1._inventory.Clear();
                }
            }
            else if (slot1Item != null && slot2Item != null)
            {
                
                //Battery
                if (slot1Item.item.id == 1 && slot2Item.item.id == 5 || slot1Item.item.id == 5 && slot2Item.item.id == 1)
                {
                    int amount = Mathf.Min(slot1Item.quantity, slot2Item.quantity);
                    output.AddItemToInventory(0, amount);
                    slot1._inventory[0].quantity -= amount;
                   
                    if (slot1._inventory[0].quantity == 0)
                        slot1._inventory.Clear();
                    slot2._inventory[0].quantity -= amount;
                    if (slot2._inventory[0].quantity == 0)
                        slot2._inventory.Clear();
                }

                //Oxygen Tank
                if (slot1Item.item.id == 7 && slot2Item.item.id == 3 || slot1Item.item.id == 3 && slot2Item.item.id == 7)
                {
                    int amount = Mathf.Min(slot1Item.quantity, slot2Item.quantity);
                    output.AddItemToInventory(8, amount);
                    slot1._inventory[0].quantity -= amount;

                    if (slot1._inventory[0].quantity == 0)
                        slot1._inventory.Clear();
                    slot2._inventory[0].quantity -= amount;
                    if (slot2._inventory[0].quantity == 0)
                        slot2._inventory.Clear();
                }

            }

            slot1.UpdateInventoryView();
            slot2.UpdateInventoryView();
            output.UpdateInventoryView();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
