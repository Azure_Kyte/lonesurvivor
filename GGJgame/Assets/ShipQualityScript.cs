﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipQualityScript : MonoBehaviour
{
    [System.Serializable]
    public class Threshold
    {
        public Mesh model;
        public float minQuality;
    }
    public List<Threshold> thresholds;
    public float currentQuality;
    public MeshFilter shipMeshFilter;


    float timer;
    bool direction;

    // Update is called once per frame
    void Update()
    {
        int id = -1;
        foreach(Threshold t in thresholds)
        {
            if (currentQuality >= t.minQuality)
                id = thresholds.IndexOf(t);
        }

        if (id > -1)
        {
            shipMeshFilter.sharedMesh = thresholds[id].model;
        }

        if (currentQuality >= 1)
        {
            SceneManager.LoadScene(1);
        }
    }
}
