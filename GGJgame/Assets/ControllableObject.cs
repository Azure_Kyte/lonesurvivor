﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllableObject : MonoBehaviour
{

    public static ControllableObject _activeObject;

    public Conditions conditionsScript;

    public bool moving;
    public float timer;

    int xPos;
    int zPos;

    int targetxPos;
    int targetzPos;

    public bool startsActive;

    float startDirection; // Up is 0, Left is 270, Right is 90, Down is 180;
    float targetDirection;

    bool isHuman;
    public bool inBase;
    public int BaseID;

    // Start is called before the first frame update
    void Awake()
    {
        xPos = Mathf.RoundToInt(transform.position.x);
        zPos = Mathf.RoundToInt(transform.position.z);
        targetzPos = zPos;
        targetxPos = xPos;

        if (startsActive)
        {
            _activeObject = this;

            ControllableObjectsUI.controllables.Add(this);
        }

        if (conditionsScript.FetchCondition(Conditions.Condition.ConditionType.Health) != null)
            isHuman = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (timer < 1)
            timer += Time.deltaTime * 5f;
        else if (timer > 1)
            timer = 1;

        if (_activeObject.GetHashCode() == GetHashCode())
        {

            if (!inBase)
                BaseOptions.mode = 0;

            if (CameraScript._target != transform)
                CameraScript._target = transform;

            bool canMove = true;

            Conditions.Condition condition = conditionsScript.FetchCondition(Conditions.Condition.ConditionType.Battery);
            if (condition != null)
                if (condition.currentValue == 0)
                    canMove = false;

            if (timer >= 1 && canMove && !inBase)
            {
                RaycastHit rch;
                if (Input.GetKey(KeyCode.W))
                {
                    Physics.Raycast(transform.position, new Vector3(0, 0, 1), out rch, 1f);
                    if (rch.collider == null)
                    {

                        xPos = Mathf.RoundToInt(transform.position.x);
                        zPos = Mathf.RoundToInt(transform.position.z);
                        targetzPos = zPos + 1;
                        targetxPos = xPos;

                        startDirection = transform.rotation.eulerAngles.y;
                        targetDirection = 0;

                        if (Mathf.Approximately(startDirection, 270f))
                            startDirection = -90;

                        timer = 0;

                        conditionsScript.ProcessConditions();
                    } else if (rch.collider.tag == "MainBase")
                    {
                        if (isHuman)
                        {
                            conditionsScript.ConditionList[0].currentValue = conditionsScript.ConditionList[0].maxValue;
                            conditionsScript.ConditionList[1].currentValue = conditionsScript.ConditionList[1].maxValue;
                            inBase = true;
                            BaseID = rch.collider.GetComponent<BaseScript>().baseID;

                            xPos = Mathf.RoundToInt(transform.position.x);
                            zPos = Mathf.RoundToInt(transform.position.z);
                            targetzPos = zPos + 1;
                            targetxPos = xPos;

                            startDirection = transform.rotation.eulerAngles.y;
                            targetDirection = 0;

                            if (Mathf.Approximately(startDirection, 270f))
                                startDirection = -90;

                            timer = 0;
                        }
                    }
                    if (rch.collider != null)
                    {
                        if (rch.collider.name.Contains("Drone"))
                        {
                            bool contained = false;
                            foreach (ControllableObject c in ControllableObjectsUI.controllables)
                            {
                                if (c.GetHashCode() == rch.collider.GetComponent<ControllableObject>().GetHashCode())
                                {
                                    contained = true;
                                }
                            }

                            if (!contained)
                                ControllableObjectsUI.controllables.Add(rch.collider.GetComponent<ControllableObject>());
                        }
                    }



                }
                else if (Input.GetKey(KeyCode.S))
                {
                    Physics.Raycast(transform.position, new Vector3(0, 0, -1), out rch, 1f);
                    if (rch.collider == null)
                    {

                        xPos = Mathf.RoundToInt(transform.position.x);
                        zPos = Mathf.RoundToInt(transform.position.z);
                        targetzPos = zPos - 1;
                        targetxPos = xPos;

                        startDirection = transform.rotation.eulerAngles.y;
                        targetDirection = 180;
                        if (Mathf.Approximately(startDirection, -90f))
                            startDirection = 270f;

                        timer = 0;

                        conditionsScript.ProcessConditions();
                    }
                    if (rch.collider != null)
                    {
                        if (rch.collider.name.Contains("Drone")){
                            bool contained = false;
                            foreach (ControllableObject c in ControllableObjectsUI.controllables)
                            {
                                if (c.GetHashCode() == rch.collider.GetComponent<ControllableObject>().GetHashCode())
                                {
                                    contained = true;
                                }
                            }

                            if (!contained)
                                ControllableObjectsUI.controllables.Add(rch.collider.GetComponent<ControllableObject>());
                        }
                    }
                }
                else if (Input.GetKey(KeyCode.A))
                {
                    Physics.Raycast(transform.position, new Vector3(-1, 0, 0), out rch, 1f);
                    if (rch.collider == null)
                    {

                        xPos = Mathf.RoundToInt(transform.position.x);
                        zPos = Mathf.RoundToInt(transform.position.z);
                        targetzPos = zPos;
                        targetxPos = xPos - 1;

                        startDirection = transform.rotation.eulerAngles.y;
                        if (startDirection <= 90)
                            targetDirection = -90;
                        else
                            targetDirection = 270;

                        timer = 0;

                        conditionsScript.ProcessConditions();
                    }
                    if (rch.collider != null)
                    {
                        if (rch.collider.name.Contains("Drone"))
                        {
                            bool contained = false;
                            foreach (ControllableObject c in ControllableObjectsUI.controllables)
                            {
                                if (c.GetHashCode() == rch.collider.GetComponent<ControllableObject>().GetHashCode())
                                {
                                    contained = true;
                                }
                            }

                            if (!contained)
                                ControllableObjectsUI.controllables.Add(rch.collider.GetComponent<ControllableObject>());
                        }
                    }
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    Physics.Raycast(transform.position, new Vector3(1, 0, 0), out rch, 1f);
                    if (rch.collider == null)
                    {

                        xPos = Mathf.RoundToInt(transform.position.x);
                        zPos = Mathf.RoundToInt(transform.position.z);
                        targetzPos = zPos;
                        targetxPos = xPos + 1;

                        startDirection = transform.rotation.eulerAngles.y;
                        targetDirection = 90;

                        timer = 0;

                        conditionsScript.ProcessConditions();
                    }
                    if (rch.collider != null)
                    {
                        if (rch.collider.name.Contains("Drone"))
                        {
                            bool contained = false;
                            foreach (ControllableObject c in ControllableObjectsUI.controllables)
                            {
                                if (c.GetHashCode() == rch.collider.GetComponent<ControllableObject>().GetHashCode())
                                {
                                    contained = true;
                                }
                            }

                            if (!contained)
                                ControllableObjectsUI.controllables.Add(rch.collider.GetComponent<ControllableObject>());
                        }
                    }
                }

            } else if (inBase)
            {
                if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
                {

                    conditionsScript.ProcessConditions();


                    conditionsScript.ConditionList[0].currentValue = conditionsScript.ConditionList[0].maxValue;
                    conditionsScript.ConditionList[1].currentValue = conditionsScript.ConditionList[1].maxValue;

                   
                    ItemSpawnController._controller.GenerateToTarget();

                    inBase = false;
                    xPos = Mathf.RoundToInt(transform.position.x);
                    zPos = Mathf.RoundToInt(transform.position.z);
                    targetzPos = zPos - 1;
                    targetxPos = xPos;

                    startDirection = 180;
                    targetDirection = 180;
                    BaseOptions.mode = 0;
                    timer = 0;
                }
            }

         
        }
        transform.rotation = Quaternion.Euler(Vector3.Lerp(new Vector3(0, startDirection, 0), new Vector3(0, targetDirection, 0), timer * 1.2f)); 
        transform.position = Vector3.Lerp(new Vector3(xPos, 0, zPos), new Vector3(targetxPos, 0, targetzPos), Mathf.SmoothStep(0f, 1f, timer));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<pickup>())
        {
            pickup p = other.GetComponent<pickup>();
            InventorySystem.Inventory._activeInventory.AddItemToInventory(p.itemID, Random.Range(p.quantMin, p.quantMax));

            Destroy(other.gameObject);
        }

        
    }

    void OnTriggerStay(Collider other)
    {

        if (_activeObject.GetHashCode() == GetHashCode())
        {
            if (other.GetComponent<LightningRod>())
            {
                Debug.Log("Lightning Rod?");
                foreach (Conditions.Condition c in _activeObject.conditionsScript.ConditionList)
                {
                    if (c.conditionType == Conditions.Condition.ConditionType.Battery)
                    {
                        c.currentValue = c.maxValue;
                        break;
                    }
                }
            }
        }
    }
}
