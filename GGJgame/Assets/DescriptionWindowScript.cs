﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DescriptionWindowScript : MonoBehaviour
{

    public static int _target = -1;
    public static InventorySystem.Inventory targetInventory;

    public Image window;
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI quantity;
    public Image icon;
    public TextMeshProUGUI description;

    int targetModified;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (_target != targetModified)
        {
            if (_target != -1)
            {
                window.enabled = true;
                itemName.enabled = true;
                quantity.enabled = true;
                icon.enabled = true;
                description.enabled = true;

                if (_target < targetInventory._inventory.Count)
                {
                    InventorySystem.Inventory.InventoryItem selectedItem = targetInventory._inventory[_target];
                    itemName.text = selectedItem.item.name;
                    quantity.text = "x" + selectedItem.quantity.ToString();
                    if (selectedItem.item.icon != null)
                        icon.sprite = selectedItem.item.icon;
                    else
                        icon.sprite = InventorySystem.ItemList._noSprite;

                    description.text = selectedItem.item.description;
                } else
                {
                    itemName.text = "No Item in Slot";
                    quantity.text = "x0";
                    icon.sprite = InventorySystem.ItemList._noItem;
                    description.text = "";
                }
            }
            else
            {
                window.enabled = false;
                itemName.enabled = false;
                quantity.enabled = false;
                icon.enabled = false;
                description.enabled = false;
            }
        }

        targetModified = _target;
       
    }
}
